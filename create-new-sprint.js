const fs = require("fs");
const path = require("path");
const prettier = require("prettier");
const { intro, outro, select, text, isCancel, cancel, spinner } = require("@clack/prompts");

const getSprints = () => {
  const pagesDir = path.join(process.cwd(), "/app/views/");
  const folders = fs.readdirSync(pagesDir);
  // Reverse the array so that the most recent sprint is first
  return folders.filter((folder) => folder.startsWith("sprint-")).reverse();
};

const duplicateFolder = (source, destination) => {
  if (!fs.existsSync(destination)) fs.mkdirSync(destination, { recursive: true });
  const files = fs.readdirSync(source);
  files.forEach((file) => {
    const current = path.join(source, file);
    const target = path.join(destination, file);
    if (fs.lstatSync(current).isDirectory()) {
      duplicateFolder(current, target);
    } else {
      fs.copyFileSync(current, target);
    }
  });
};

const updateIndexFile = async (newSprintName, newSprintTitle) => {
  const indexPath = path.join(process.cwd(), "/app/views/index.html");
  const indexContent = fs.readFileSync(indexPath, "utf-8");
  const lines = indexContent.split("\n");
  const today = new Date();
  const day = today.getDate();
  const month = today.getMonth();
  const year = today.getFullYear();
  const dateString = new Date(year, month, day).toDateString();
  const newPrototypeCard = `
    {{ macros.prototypeCard(
      title="${newSprintTitle}",
      tagText="${newSprintName.toUpperCase()}",
      href="/${newSprintName}/start",
      dateCreated="${dateString}"
    ) }}
  `;
  // Find the line number where we want to insert the new PrototypeCard
  const insertionLine = lines.findIndex((line) => line.trim() === "{{ macros.prototypeCard(");
  // Insert the newPrototypeCard at the found index
  lines.splice(insertionLine, 0, newPrototypeCard);

  // Write the file back, using prettier to format it
  fs.writeFileSync(indexPath, lines.join("\n"), { semi: false, parser: "babel" }, "utf-8");
};

// Main function
const main = async () => {
  intro(`Sprint Cloner`);

  const sprints = getSprints();
  const sprintToClone = await select({
    message: "Select a sprint to clone:",
    options: sprints.map((sprint) => ({ value: sprint, label: sprint })),
  });

  if (isCancel(sprintToClone)) {
    cancel("Operation cancelled.");
    process.exit(0);
  }

  const newSprintName = await text({
    message:
      "Enter a name for the new sprint. This is used for the folder names so use dashes - to separate:",
    initialValue: `sprint-`,
  });

  if (isCancel(newSprintName)) {
    cancel("Operation cancelled.");
    process.exit(0);
  }

  const newSprintTitle = await text({
    message: "Enter a title for the new sprint. This is used as the heading on the index page:",
  });

  if (isCancel(newSprintTitle)) {
    cancel("Operation cancelled.");
    process.exit(0);
  }

  const s = spinner();
  s.start("Cloning sprint...");

  const sourceViews = path.join(process.cwd(), `/app/views/${sprintToClone}`);
  const destinationViews = path.join(process.cwd(), `/app/views/${newSprintName}`);

  const sourceConfigFiles = path.join(process.cwd(), `/app/page-configurations/${sprintToClone}`);
  const destinationConfigFiles = path.join(
    process.cwd(),
    `/app/page-configurations/${newSprintName}`,
  );

  duplicateFolder(sourceViews, destinationViews);
  duplicateFolder(sourceConfigFiles, destinationConfigFiles);
  s.stop("Sprint cloned.");

  await updateIndexFile(newSprintName, newSprintTitle);

  outro(`You're all set!`);
};

main();
