const { conditionMatch } = require("./condition-match");
const logger = require("../lib/logger");

module.exports.handleRouting = (config, formData) => {
  let nextPage = config.next;
  if (Array.isArray(nextPage)) {
    logger.debug("Next page is an array of options ...");
    nextPage = conditionMatch(config.next, formData);
  } else {
    logger.debug(`Next page is a single option '${nextPage}'`);
  }
  return nextPage;
};
