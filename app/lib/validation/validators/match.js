const { validationError } = require("../../validation-error");

class MatchValidator {
  constructor(options, errors) {
    this.valid = true;
    this.options = {
      required: true,
      maxLength: undefined,
      minLength: undefined,
    };

    Object.assign(this.options, options);

    this.errors = {
      required: validationError("You must enter a value."),
      mustMatch: validationError("You must enter a matching value."),
    };

    Object.assign(this.errors, errors);
  }

  validate(confirmationValue, firstValue) {
    if (this.options.required && !confirmationValue) {
      this.valid = false;
      this.error = this.errors.required;
    }

    if (this.valid !== false && confirmationValue !== firstValue) {
      this.error = this.errors.mustMatch;
    }

    return this.error;
  }
}

module.exports = MatchValidator;
