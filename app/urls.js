module.exports.urls = {
  start: "start",
  fullName: "full-name",
  dateOfBirth: "date-of-birth",
  dateOfBirthIneligible: "date-of-birth-ineligible",
  contactPreferences: "contact-preferences",
  checkYourAnswers: "check-your-answers",
};
