//
// For guidance on how to create filters see:
// https://prototype-kit.service.gov.uk/docs/filters
//

const govukPrototypeKit = require('govuk-prototype-kit')
const addFilter = govukPrototypeKit.views.addFilter
const { parse, format, isValid } = require('date-fns');

/**
 * Format a date from separate parts into a readable format.
 * Default output format is 01 January 2023
 */
addFilter("formatDate", (data, datePrefix, outputFormat = 'dd MMMM yyyy') => {
  // Extract date parts from the session data using the prefix
  const day = data[`${datePrefix}-day`];
  const month = data[`${datePrefix}-month`];
  const year = data[`${datePrefix}-year`];

  // Attempt to create a date which we can then format
  const dateToFormat = parse(`${day}/${month}/${year}`, 'dd/MM/yyyy', new Date());

  // Log an error if invalid date
  if (!isValid(dateToFormat)) {
    console.error("formatDate was called with an invalid date")
    return
  }

  // Return the formatted date
  return format(dateToFormat, outputFormat);
})
