const { urls } = require("../../urls");
const { fieldTypes } = require("../../lib/field-types");
const { validationError } = require("../../lib/validation-error");

/** @type {import('../../lib/types').Config} */
const config = {
  previous: urls.start,
  next: urls.dateOfBirth,
  validation: {
    fullName: {
      type: fieldTypes.textInput,
      errors: {
        required: validationError("Enter your full name."),
      },
    },
  },
};

module.exports = config;
