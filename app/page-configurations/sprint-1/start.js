const { urls } = require("../../urls");

/** @type {import('../../lib/types').Config} */
const config = {
  next: urls.fullName,
};

module.exports = config;
